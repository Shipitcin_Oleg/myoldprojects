//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>
#include <InitOGL.h>
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

HDC dc;
bool  keys[256];                		// ������, ������������ ��� �������� � �����������
bool  active=true;                		// ���� ���������� ����, ������������� � true �� ���������
bool  fullscreen=true;            		// ���� ������ ����, ������������� � ������������� �� ���������

BOOL light;      // ���� ��� / ����
BOOL lp;         // L ������?
BOOL fp;         // F ������?

GLfloat xrot = 0;         // X ��������
GLfloat zrot;
GLfloat yrot;         // Y ��������
GLfloat xspeed;       // X �������� ��������
GLfloat yspeed;       // Y �������� ��������

//GLfloat z=-50.0f;      // ����� ������ ������

double x = 0.0, y = 0.0, z= 110.0; //��������� ������ � ������������
float angleX=15.0, angleY=-45.0; // ���� �������� ������
POINT mousexy; // ���� �� ����� ��������� ���� �� ������.
float aX, aY;

float b = 1.0;

double l;

bool normal = false;
bool sca = false;
bool d = false;
const double PI = 3.1415;
int px=0, py=0;

GLfloat LightAmbient[]= { 0.5f, 0.5f, 0.5f, 1.0f }; // �������� �������� ����� ( ����� )

GLfloat LightDiffuse[]= { 1.0f, 0.0f, 1.0f, 1.0f }; // �������� ���������� ����� ( ����� )
GLfloat LightPosition[]= { 0.0f, 0.0f, 2.0f, 0.0f };     // ������� ����� ( ����� )

GLUquadricObj *quadratic;    // ����� ��� �������� ������� Quadratic

GLuint filter;         // ����� ������ ������������
GLuint texture[3];     // ����� ��� �������� 3 �������


GLuint m_lstCube;
GLfloat m_matDiffuse[] = {0.0f, 0.0f, 0.0f};


//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


struct sCogwheel
{
	double r;  // ������ ���������� ������
	int z;     // ���-�� ������
	double a;  // ������ ������� ����� ����
	double b;  // ����� ����
	double c;  // ���������� ����� ����� �� ������
	double w;  // ������ ������
	double h;  // ������ ����
	double p;  // ��� ������
	double m;  // ������ ����������
	bool create;   // ���� ����������� ������

	int mx, my;
	int pos;    // ������ � ������� data

	double wSpeed;
	bool clock;
	double angle;

	GLfloat *data; // ������ � ������ ��� ���������
	GLuint size;   // ������ ������� ������


	sCogwheel()
	{
		r = 0.0;
		z = 0;
		a = 0.0;
		b = 0.0;
		c = 0.0;
		h = 0.0;
		w = 0.0;
		m = 0.0;
		pos = 0;
		create = false;
		data = NULL;
		size = 0;
		angle = 0;
		wSpeed = 0.0;
	}

	void GetM(double M)
	{
		if (M <= 0.6) {
			m = 0.5;
			return;
		}
		if (M > 0.6 && M <= 0.85) {
			m = 0.7;
			return;
		}
		if (M > 0.85 && M <= 1.125) {
			m = 1.0;
			return;
		}
		if (M > 1.125 && M <= 1.375) {
			m = 1.25;
			return;
		}
		if (M > 1.375 && M <= 1.625) {
			m = 1.5;
			return;
		}
		if (M > 1.625 && M <= 1.875) {
			m = 1.75;
			return;
		}
		if (M > 1.875 && M <= 2.25) {
			m = 2.0;
			return;
		}
		if (M > 50.0) {
			m = 50.0; return;
		}
		if (M > 2.0) {
			int tmp = M;
			if ( ((M+0.5) - tmp) >= 1.0 ) {
				m = (double)(tmp) + 0.5;
			}
			else
			{
				m = (double)tmp;
			}
		}
	}

	void CalcParam(double radius)
	{
		double d = 2*radius;
		GetM(radius /20.0);
		z = d / m;
		r = (m * z) / 2.0;
		d = r + r;
		p = (double) (d*3.1415) / ((double) z);
		a = 0.3 * p;
		b = ((p - a) / 2.0) * 0.9 ;
		c = (p - a - b);
		h = 2.25 * m;
		w = 0.5 * r;
		angle = 0;
		wSpeed = 0.0;
	}

	void CalcParam(double radius, int zub)
	{
		double d = 2*radius;
		GetM( (radius*2.0) / zub );
		z = d / m;
		r = (m * z) / 2.0;
		d = r + r;
		p = (double) (d*3.1415) / ((double) z);
		a = 0.3 * p;
		b = ((p - a) / 2.0) * 0.9 ;
		c = (p - a - b);
		h = 2.25 * m;
		w = 0.5 * r;
		angle = 0;
		wSpeed = 0.0;
	}

	void CalcParam(double radius, double mod)
	{
		double d = 2*radius;
		GetM( mod );
		z = d / m;
		r = (m * z) / 2.0;
		d = r + r;
		p = (double) (d*3.1415) / ((double) z);
		a = 0.3 * p;
		b = ((p - a) / 2.0) * 0.9 ;
		c = (p - a - b);
		h = 2.25 * m;
		w = 0.5 * r;
		angle = 0;
		wSpeed = 0.0;
	}

	void CalcParam(double radius, double mod, double ww)
	{
		double d = 2*radius;
		GetM( mod );
		z = d / m;
		r = (m * z) / 2.0;
		d = r + r;
		p = (double) (d*3.1415) / ((double) z);
		a = 0.2 * p;
		b = ((p - a) / 2.0) * 0.9 ;
		c = (p - a - b);
		h = 2.25 * m;
		w = ww;
		angle = 0;
		wSpeed = 0.0;
	}

	void WriteData(double x, double y, double z)
	{
		data[pos] = x;
		data[pos+1] = y;
		data[pos+2] = z;
		pos += 3;
	}

	bool BuildData()
	{
		size = z * 96;
		data = new float[size];
		GLfloat xx = 0.0;
		GLfloat yy = 0.0;
		GLfloat zz = 0.0;

		//a = 20;

		double fi = double (360.0 / (z));
		double x1, y1, x2, y2, x3, y3, x4, x5, y4, y5;
		double cc = sqrt((r+h)*(r+h) + b*b);
		double c2 = sqrt((r+h)*(r+h) + (b+a)*(b+a));


		x1 = 0.0;
		y1 = r;

		x2 = ( ((r+h)*b) / cc );
		y2 = ( cc - (sqrt(b*b - x2*x2)));

		x3 = ( ((r+h)*(b+a)) / c2);
		y3 = ( c2 - (sqrt( ((b+a)*(b+a)) - x3*x3)));

		double k = 3.1415 / 180.0;

		x4 = ( cos( k * (90.0 - ( 0.75 * fi))) * r);
		y4 = ( sin( k * (90.0 - ( 0.75 * fi))) * r);

		x5 = ( cos( k * (90.0 - fi)) * r);
		y5 = ( sin( k * (90.0 - fi)) * r);


		pos = 0;

		double xx1, yy1, xx2, yy2, xx3, yy3, xx4, xx5, yy4, yy5;
		double f = fi;

		for (int i = 0; i < z; i++)
		{

			xx1 = x1 * cos(k*fi) + y1 * sin(k*fi); yy1 = -x1 * sin(k*fi) + y1 * cos(k*fi);
			xx2 = x2 * cos(k*fi) + y2 * sin(k*fi); yy2 = -x2 * sin(k*fi) + y2 * cos(k*fi);
			xx3 = x3 * cos(k*fi) + y3 * sin(k*fi); yy3 = -x3 * sin(k*fi) + y3 * cos(k*fi);
			xx4 = x4 * cos(k*fi) + y4 * sin(k*fi); yy4 = -x4 * sin(k*fi) + y4 * cos(k*fi);
			xx5 = x5 * cos(k*fi) + y5 * sin(k*fi); yy5 = -x5 * sin(k*fi) + y5 * cos(k*fi);
				   //  0
			WriteData(xx1, yy1, zz); WriteData(xx2, yy2, zz);
			WriteData(xx3, yy3, zz); WriteData(xx4, yy4, zz);  // 11

			WriteData(xx1, yy1, zz); WriteData(xx1, yy1, zz - w);
			WriteData(xx2, yy2, zz - w); WriteData(xx2, yy2, zz);  // 23

			WriteData(xx2, yy2, zz); WriteData(xx2, yy2, zz-w);
			WriteData(xx3, yy3, zz-w); WriteData(xx3, yy3, zz);   // 35


			WriteData(xx3, yy3, zz); WriteData(xx3, yy3, zz-w);
			WriteData(xx4, yy4, zz-w); WriteData(xx4, yy4, zz);   // 47

			WriteData(xx1, yy1, zz-w); WriteData(xx2, yy2, zz-w);
			WriteData(xx3, yy3, zz-w); WriteData(xx4, yy4, zz-w); // 59

			WriteData(xx4, yy4, zz); WriteData(xx4, yy4, zz-w);
			WriteData(xx5, yy5, zz-w); WriteData(xx5, yy5, zz);   // 71

			WriteData(0.0, 0.0, zz-w); WriteData(xx1, yy1, zz-w);
			WriteData(xx4, yy4, zz-w); WriteData(xx5, yy5, zz-w); // 83

			WriteData(0.0, 0.0, zz); WriteData(xx1, yy1, zz);
			WriteData(xx4, yy4, zz); WriteData(xx5, yy5, zz);     // 95

			fi += f;

		}


	}



	sCogwheel(double radius)
	{
		CalcParam(radius);
		BuildData();

	}

	sCogwheel(double radius, int zub)
	{
		CalcParam(radius, zub);
		BuildData();
	}

	sCogwheel(double radius, double modul)
	{
		CalcParam(radius, modul);
		BuildData();

	}

	sCogwheel(double radius, double modul, double ww)
	{
		CalcParam(radius, modul, ww);
		BuildData();
	}


	DrawPlan(TImage *I, double r1, double h1)
	{
		TCanvas *C = I->Canvas;
		C->Rectangle(-2,-2,I->Width+2, I->Height+2);
		int x0 = I->Width / 4 ;
		int y0 = I->Height / 2;
		int min = x0;
		if (y0 < x0) {
			min = y0;
		}
		double k = min/(1.4 * r);



		C->Ellipse(x0 - r1*k, y0 - r1*k, x0 + r1*k, y0 + r1*k);
        C->MoveTo(x0, y0);
		C->LineTo(x0 + r*k*1.2,y0 + 1.2*r*k);
		C->LineTo(x0 + r*k*1.2 + r*k*0.4,y0 + 1.2*r*k);
		C->TextOutW(x0 + r*k*1.2 + r*k*0.15, y0 + 1.05*r*k, FloatToStrF(r1,ffGeneral, 3,2));



		C->MoveTo(x0 + data[0]*k, y0+ data[1]*k);
		//int i=0;
		for(int i = 0; i < size; i+=96)
		{
			C->LineTo(x0 + data[0+i]*k, y0+data[1+i]*k);
			C->LineTo(x0 + data[3+i]*k, y0+data[4+i]*k);
			C->LineTo(x0 + data[6+i]*k, y0+data[7+i]*k);
			C->LineTo(x0 + data[9+i]*k, y0+data[10+i]*k);
		}
		C->LineTo(x0 + data[0]*k, y0+data[1]*k);

		C->MoveTo(x0, y0);
		C->LineTo(x0 + r*k*1.2,y0 - 1.2*r*k);
		C->LineTo(x0 + r*k*1.2 + r*k*0.4,y0 - 1.2*r*k);
		C->TextOutW(x0 + r*k*1.2 + r*k*0.15, y0 - 1.35*r*k, FloatToStrF(r,ffGeneral, 3,2));


		C->MoveTo(x0 , y0-data[1]*k);
		C->LineTo(x0 + b*k - r*k*1.3, y0-r*k);
		C->MoveTo(x0 + b*k, y0-data[4]*k);
		C->LineTo(x0 + b*k - r*k*1.3, y0-data[4]*k);

		C->MoveTo(x0 + b*k - r*k*1.3, y0-data[4]*k + h*k*3.0);
		C->LineTo(x0 + b*k - r*k*1.3, y0-data[4]*k - h*k*3.0);
		C->LineTo(x0 + b*k - r*k*1.0, y0-data[4]*k - h*k*3.0);
		C->TextOutW(x0 + b*k - r*k*1.25, y0-data[4]*k - h*k*3.0 - 14.0 , FloatToStrF(h,ffGeneral, 3,2));


		C->MoveTo(x0 + b*k - r*k*1.3 - 4, y0-data[4]*k + h*k + 8);
		C->LineTo(x0 + b*k - r*k*1.3, y0-data[4]*k + h*k);
		C->LineTo(x0 + b*k - r*k*1.3 + 5, y0-data[4]*k + h*k + 9);

		C->MoveTo(x0 + b*k - r*k*1.3 - 4, y0-data[4]*k - 8);
		C->LineTo(x0 + b*k - r*k*1.3, y0-data[4]*k);
		C->LineTo(x0 + b*k - r*k*1.3 + 5, y0-data[4]*k - 9);


		k = k / 1.41421;
		C->MoveTo(x0 + r*k - 6, y0 - r*k + 10);
		C->LineTo(x0 + r*k,y0 - r*k);
		C->LineTo(x0 + r*k - 11,y0 - r*k + 7);

		C->MoveTo(x0 + r1*k - 5, y0 + r1*k - 10);
		C->LineTo(x0 + r1*k, y0 + r1*k);
		C->LineTo(x0 + r1*k - 10,y0 + r1*k - 5);
		k = k * 1.41421;

		x0 += x0 + x0;

		C->Rectangle(x0 - r1*k, y0 - h1*k, x0 + r1*k, y0 + h1*0.5*k);

		C->MoveTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k + (w*k)/2.0);
		C->LineTo(x0 + data[4]*k, y0 + data[2]*k + w*k/2.0);

		C->MoveTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k - w*k/2.0);
		C->LineTo(x0 + data[4]*k, y0 + data[2]*k - w*k/2.0);

		C->MoveTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k + w*k*1.4);
		C->LineTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k - w*k*1.8);
		C->LineTo(x0 - data[4]*k + r*k*0.2, y0 + data[2]*k - w*k*1.8);
		C->TextOutW(x0 - data[4]*k - r*k*0.15, y0 + data[2]*k - w*k*1.8 - 14 , FloatToStrF(w,ffGeneral, 3,2));


		C->MoveTo(x0 - data[4]*k - r*k*0.2 - 4, y0 + data[2]*k - w*k/2.0 - 10);
		C->LineTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k - w*k/2.0);
		C->LineTo(x0 - data[4]*k - r*k*0.2 + 4, y0 + data[2]*k - w*k/2.0 - 10);

		C->MoveTo(x0 - data[4]*k - r*k*0.2 - 4, y0 + data[2]*k + w*k/2.0 + 10);
		C->LineTo(x0 - data[4]*k - r*k*0.2, y0 + data[2]*k + w*k/2.0);
		C->LineTo(x0 - data[4]*k - r*k*0.2 + 4, y0 + data[2]*k + w*k/2.0 + 10);

		double x;
		double y;

		for(int i = 0; i < size/2; i+=96)
		{

			x = x0 + data[16+i]*k;
			y = y0 + data[2]*k;
			C->MoveTo(x, y + (w*k)/2.0);
			C->LineTo(x, y - (w*k)/2.0);
			C->MoveTo(x = x0 + data[4+i]*k, y + (w*k)/2.0);
			C->LineTo(x = x0 + data[4+i]*k, y - (w*k)/2.0);

			//C->LineTo(x0 + data[4+i]*k, y0+data[5+i]*k);
			//C->LineTo(x0 + data[7+i]*k, y0+data[8+i]*k);
			//C->LineTo(x0 + data[10+i]*k, y0+data[11+i]*k);
		} //         */
	}

	DrawCogwheel()
	{
		glColor3f(0.5,0.5,0.5);
		glRotated(angle,0.0,0.0,1.0);
			glBegin(GL_QUADS);

            		for(int i = 0; i < size; i+=12)
            		{
            			if ( ((i % 96) == 12) || ((i % 96) == 36)) {
            				glColor3f(0.3,0.3,0.3);
            			}
            			else
            			{
            				glColor3f(0.5,0.5,0.5);
            			}
            			for(int j = 0; j < 4; j++)
            			{
            				glVertex3d(data[i+(j*3)],data[i+1+(j*3)],data[i+2+(j*3)] );
            			}
					}
					glEnd();



		glColor3f(0.0,0.0,0.0);
		glLineWidth(2.0);
		glBegin(GL_LINES);

		glVertex3d(data[0],data[1],data[2] );
		glVertex3d(data[84],data[85],data[86] );

		for(int i = 0; i < size; i+=96)
		{
		glVertex3d(data[0+i],data[1+i],data[2+i] );
		glVertex3d(data[3+i],data[4+i],data[5+i] );

		glVertex3d(data[3+i],data[4+i],data[5+i] );
		glVertex3d(data[6+i],data[7+i],data[8+i] );

		glVertex3d(data[6+i],data[7+i],data[8+i] );
		glVertex3d(data[9+i],data[10+i],data[11+i] );

		glVertex3d(data[0+i],data[1+i],data[2+i] );
		glVertex3d(data[15+i],data[16+i],data[17+i]);

		glVertex3d(data[15+i],data[16+i],data[17+i]);
		glVertex3d(data[18+i],data[19+i],data[20+i]);

		glVertex3d(data[18+i],data[19+i],data[20+i]);
		glVertex3d(data[3+i],data[4+i],data[5+i] );

		glVertex3d(data[27+i],data[28+i],data[29+i]);
		glVertex3d(data[30+i],data[31+i],data[32+i]);

		glVertex3d(data[30+i],data[31+i],data[32+i]);
		glVertex3d(data[33+i],data[34+i],data[35+i]);


		glVertex3d(data[42+i],data[43+i],data[44+i]);
		glVertex3d(data[45+i],data[46+i],data[47+i]);


		glVertex3d(data[54+i],data[55+i],data[56+i]);
		glVertex3d(data[57+i],data[58+i],data[59+i]);

		glVertex3d(data[63+i],data[64+i],data[65+i]);
		glVertex3d(data[66+i],data[67+i],data[68+i]);

		glVertex3d(data[66+i],data[67+i],data[68+i]);
		glVertex3d(data[69+i],data[70+i],data[71+i]);

		glVertex3d(data[90+i],data[91+i],data[92+i]);
		glVertex3d(data[93+i],data[94+i],data[95+i]);



					}
		glEnd();
		glRotated(-angle,0.0,0.0,1.0);
		if (clock) {angle += wSpeed;} else {angle -= wSpeed;}
		glColor3f(0.5,0.5,0.5);
	}

};



struct sPart
{
	sCogwheel *cog1;
	sCogwheel *cog2;

	double r1, h1; // ��������� ����� ������
	double r2, h2;

	sPart()
	{
		cog1 = new sCogwheel(3, 0.5);
		cog2 = new sCogwheel(8, 1.0);

		r1 = cog1->r * 0.2; h1 = cog1->h;
		r2 = cog2->r * 0.2; h2 = cog2->h;
	}

	sPart(double radius1, double w1, double modul1, double radius2, double w2, double modul2)
	{
		cog1 = new sCogwheel(radius1, modul1, w1);
		cog2 = new sCogwheel(radius2, modul2, w2);

		r1 = cog1->r * 0.2; h1 = cog1->h;
		r2 = cog2->r * 0.2; h2 = cog2->h;
	}

    void DrawCircle(float z, float r, int amountSegments)
	{

	 glColor3f(0.0,0.0,0.0);
	 glLineWidth(2.0);
	 glBegin(GL_LINE_LOOP);
	 r+= 0.01;
	 for(int i = 0; i < amountSegments; i++)
	 {
	  float angle = 2.0 * 3.1415926 * float(i) / float(amountSegments);

	  float dx = r * cos(angle);
	  float dy = r * sin(angle);

	  glVertex3f(dx, dy, z);
	}

	 glEnd();
	}

	DrawPart()
	{

		glRotated(cog1->angle,0.0,0.0,1.0);
		DrawCircle(0.01,r1,24);
		DrawCircle(h1,r1,24);
		glBegin(GL_LINES);
			glVertex3d(0,r1,0);
			glVertex3d(0,r1,h1);
		glEnd();
		glRotated(-cog1->angle,0.0,0.0,1.0);
		cog1->DrawCogwheel();
		glTranslatef(0.0f,0.0f,-cog1->w);

		cog2->DrawCogwheel();
		glTranslatef(0.0f,0.0f, cog1->w  );

		glColor3f(0.35,0.35,0.35);
		gluCylinder(quadratic, r1,r1,h1,32,32);
		glTranslatef(0.0f,0.0f,-(cog1->w + cog2->w+ h2));
		gluCylinder(quadratic, r2,r2,h2,32,32);
		DrawCircle(0.0,r2,24);
		DrawCircle(h2-0.01,r2,24);


	}


};

struct sShaft
{
	sCogwheel *cog;

	double dz;

	double r1, h1; // ��������� ���� ������
	double r2, h2;


	sShaft()
	{
		cog = NULL;

		dz = 0.0;
		r1 = 0.0; h1 = 0.0;
		r2 = 0.0; h2 = 0.0;
	}

	sShaft(double radiusShaft, double heightShaft, double radius,  double w, double modul, double _dz = 0.0)
	{
		cog = new sCogwheel(radius, modul, w);
		dz = _dz;
		r1 = radiusShaft; h1 = heightShaft;
		r2 = radiusShaft; h2 = w;
	}


	void DrawCircle(float z, float r, int amountSegments)
	{

	 glColor3f(0.0,0.0,0.0);
	 glLineWidth(2.0);
	 glBegin(GL_LINE_LOOP);
	 r+= 0.01;
	 for(int i = 0; i < amountSegments; i++)
	 {
	  float angle = 2.0 * 3.1415926 * float(i) / float(amountSegments);

	  float dx = r * cos(angle);
	  float dy = r * sin(angle);

	  glVertex3f(dx, dy, z);
	}

	 glEnd();
	}



	DrawShaft()
	{

		glTranslatef(0.0f,0.0f, -dz);
		cog->DrawCogwheel();
		glRotated(cog->angle,0.0,0.0,1.0);


		DrawCircle(0.1,r1,24);
		DrawCircle(h1,r1,24);
		glBegin(GL_LINES);
			glVertex3d(0,r1,0);
			glVertex3d(0,r1,h1);
		glEnd();
		glColor3f(0.35,0.35,0.35);
		glTranslatef(0.0f,0.0f, 0 );
		gluCylinder(quadratic, r1,r1,h1,24,24);
		glTranslatef(0.0f,0.0f,-(cog->w + h2));
		gluCylinder(quadratic, r2,r2,h2,24,24);
		DrawCircle(0.0,r2,24);
		DrawCircle(h2-0.1,r2,24);
		glRotated(-cog->angle,0.0,0.0,1.0);

	}

};




AUX_RGBImageRec *LoadBMP(char *Filename)     // �������� ��������
{
 FILE *File=NULL;          // ������ �����

 if (!Filename)            // �������� ����� �����
 {
  return NULL;             // ���� ��� ������ NULL
 }

 File=fopen(Filename,"r"); // �������� ���������� �� ����

 if (File)                 // ���� ����������?
 {
  fclose(File);            // ������� ����
  return auxDIBImageLoad(WideString(Filename).c_bstr()); // �������� �������� � ������ �� ��� ���������
 }
 return NULL;              // ���� �������� �� ������� ������ NULL
}


GLvoid ReSizeGLScene( GLsizei width, GLsizei height )        // �������� ������ � ���������������� ���� GL
{
	if( height == 0 )              // �������������� ������� �� ����
	{
		height = 1;
	}

	glViewport( 0, 0, width, height );          // ����� ������� ������� ������
	glMatrixMode( GL_PROJECTION );            // ����� ������� ��������
	glLoadIdentity();              // ����� ������� ��������

	// ���������� ����������� �������������� �������� ��� ����
	gluPerspective( 45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 1000.0f );
	//glOrtho(-1000,1000,-1000,1000,-1000,1000);
	glMatrixMode( GL_MODELVIEW );            // ����� ������� ���� ������
	glLoadIdentity();              // ����� ������� ���� ������
}


int LoadGLTextures()                      // �������� �������� � ��������������� � ��������
{
 int Status=false;                        // ��������� ���������

 AUX_RGBImageRec *TextureImage[1];        // ������� ����� ��� ��������

 memset(TextureImage,0,sizeof(void *)*1); // ���������� ��������� � NULL
	// �������� ��������, �������� �� ������, ���� �������� �� ������� - �����
	 if (TextureImage[0]=LoadBMP("t2.bmp"))
	 {
		  Status=true;       // ��������� Status � TRUE
		  glGenTextures(3, &texture[0]);     // �������� ���� �������
		  // �������� �������� � �������� �� �������� ��������
		  glBindTexture(GL_TEXTURE_2D, texture[0]);
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); // ( ����� )
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); // ( ����� )
		  glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY,
		   0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);

		   // �������� �������� � �������� �����������
		  glBindTexture(GL_TEXTURE_2D, texture[1]);
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		  glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY,
		   0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);

		  // �������� �������� � ���-����������
		  glBindTexture(GL_TEXTURE_2D, texture[2]);
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST); // ( ����� )
		  gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY,
			GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data); // ( ����� )
	 }

	 if (TextureImage[0])           // ���� �������� ����������
	 {
	  if (TextureImage[0]->data)    // ���� ����������� �������� ����������
	  {
	   free(TextureImage[0]->data); // ������������ ������ ����������� ��������
	  }

	  free(TextureImage[0]);        // ������������ ������ ��� ���������
	 }

	 return Status;        // ���������� ������

}


//*************************************************************
//-------------------------------------------------------------
//*************************************************************

//sPart sp(8.0, 6.0, 0.7,  11.0, 4.0, 0.5);

//sShaft sh1(6.0, 10.0, 13.0, 6.0, 0.7);

//sShaft sh2(5.0, 20.0, 20.0, 4.0, 0.5, sp.cog1->w);

sPart *sp;
sShaft *sh1;
sShaft *sh2;

//-------------------------------------------------------------
//-------------------------------------------------------------
//*************************************************************


int InitGL( GLvoid )                // ��� ��������� ������� OpenGL ���������� �����
{
	//if (!LoadGLTextures())        // ������� �� ��������� �������� ��������
	// {
	//  return false;                // ���� �������� �� ��������� ���������� FALSE
	// }

	//glEnable(GL_TEXTURE_2D);      // ��������� ��������� ��������
	glOrtho(-5000,5000,-5000,5000,-5000,5000);
	glShadeModel( GL_SMOOTH );            // ��������� ������� �������� �����������
	glClearColor(1.0f, 1.0f, 1.0f, 0.5f);          // ���� ������� ������
	glClearDepth( 1.0f );              // ��������� ������� ������ �������
    glEnable( GL_DEPTH_TEST );            // ��������� ���� �������
	glDepthFunc( GL_LEQUAL );            // ��� ����� �������
	///*
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 0);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m_matDiffuse);

	m_matDiffuse[0] = 1.0;
	m_matDiffuse[1] = 0.4;
	m_matDiffuse[2] = 0.6;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m_matDiffuse);
	glEnable(GL_COLOR_MATERIAL);       // */

	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );      // ��������� � ���������� �����������

    quadratic=gluNewQuadric();     // ������� ��������� �� ������������ ������ ( ����� )
	gluQuadricNormals(quadratic, GLU_SMOOTH); // ������� ������� ������� ( ����� )
	return true;                // ������������� ������ �������
}

int DrawGLScene()                // ����� ����� ����������� ��� ����������
{
	  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );      // �������� ����� � ����� �������




	  glLoadIdentity();              // �������� ������� �������


	  if (light)               // ���� �� ����
	  {
		glDisable(GL_LIGHTING);  // ������ ���������
	  }



	  //glColor3f(0.5,0.5,0.5);
	   glBegin(GL_QUADS);
		glColor3f(1.0,1.0,1.0);
		glVertex3d(-1500,200,-(1000));
		glVertex3d(1500,200,-(1000));

		glColor3f(0.1,0.1,1.0);

		glVertex3d(1500,-400,-(1000));
		glVertex3d(-1500,-400,-(1000));
	  glEnd();
	  if (light) glEnable(GL_LIGHTING);

	  glTranslatef(0.0f,0.0f,-z);  // ������� �/��� ������ �� z
	  //glScalef(b,b,b);
	  //gluLookAt(xrot,z + yrot,-z , 0,0,0, 0,1,0);

	  //GetCursorPos(&mousexy);
	  //	angleX += (px - mousexy.x) /3; //2 � ����������������
	  //	angleY += (py - mousexy.y) /3;
	  //	SetCursorPos(px,py); // ��������� ��������� ����


	  //gluLookAt(x,y,z,x-sin(angleX/180*PI),y+(tan(angleY/180*PI)),z-cos(angleX/180*PI), 0, 1, 0);
	  glRotatef(angleY,1.0f,0.0f,0.0f); // �������� �� ��� X �� xrot
	  glRotatef(angleX,0.0f,0.0f,1.0f); // �������� �� ��� Y �� yrot
	  //glRotatef(angleX+angleY,0.0f,0.0f,1.0f);


	  glPushMatrix();
	  //glLightfv(GL_LIGHT1, GL_POSITION, LightPosition);
	  //glRotatef(zrot,0.0f,0.0f,1.0f);
	  sp->DrawPart();

	  glPopMatrix();

	  glPushMatrix();
	  glTranslatef(sp->cog1->r + sh1->cog->r +  ((sp->cog1->h + sh1->cog->h)/2.0),0.0f,0.0f);
	  sh1->DrawShaft();

	  glPopMatrix();
	  glTranslatef(-( sp->cog2->r + sh2->cog->r +  ((sp->cog2->h + sh2->cog->h)/2.0)),0.0f,0.0f);
	  sh2->DrawShaft();
	  //glPopMatrix();

	  //glBindTexture(GL_TEXTURE_2D, texture[filter]);    // ����� �������� ����������� �� filter
/*

		  glColor3f(0.5,0.5,0.5);
    	  glBegin(GL_QUADS);
    		for(int i = 0; i < s.size; i+=12)
    		{
    			if ( ((i % 96) == 12) || ((i % 96) == 36)) {
    				glColor3f(0.3,0.3,0.3);
    			}
    			else
    			{
    				glColor3f(0.5,0.5,0.5);
    			}
    			for(int j = 0; j < 4; j++)
    			{
    				glVertex3d(s.data[i+(j*3)],s.data[i+1+(j*3)],s.data[i+2+(j*3)] );
    			}
			}
		  glEnd();
*/

	   SwapBuffers( dc);

		//zrot+= Form1->TrackBar1->Position/ 5.0;
		//Form1->Caption = IntToStr(Form1->TrackBar1->Position);
		//xrot+=xspeed;        // �������� � xspeed �������� xrot
		//yrot+=yspeed;        // �������� � yspeed �������� yrot

	  return true;                // ���������� ������ �������
}



void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Form1->BorderStyle = TFormBorderStyle::bsSingle;
	TBorderIcons tempBI = BorderIcons;
	tempBI >> biMaximize;
	Form1->BorderIcons = tempBI;
	dc = SetupContext(Form1->Panel1->Handle);
	InitGL();

   FormResize(Sender);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject *Sender)
{

	Form1->Button1->Left = Form1->Width - Form1->Button1->Width - 40;
	Form1->Button1->Top = Form1->Height - Form1->Button1->Height - 50;
	ReSizeGLScene(Width, Height);
	return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	DeleteContext();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	if (d)     // �������� �����
		{
		GetCursorPos(&mousexy);
		angleX -= (px - mousexy.x) /3; //2 � ����������������
		angleY -= (py - mousexy.y) /3;
		//SetCursorPos(px,py); // ��������� ��������� ����

		GetCursorPos(&mousexy);
        px = mousexy.x;
		py = mousexy.y;
		sca = true;
		}
	DrawGLScene();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject *Sender, System::WideChar &Key)
{


	if (Key == 'F' || Key == 'f' || Key == '�' || Key == '�') {
		filter+=1;           // �������� filter ������������� �� ����
		 if (filter>2)        // �������� ������ ��� 2?
		 {
			filter=0;           // ���� ���, �� ��������� filter � 0
		 }
	}

	if (Key == '8') {
		xspeed-=0.1f;
	}
	if(Key == '5')
	{
		xspeed+=0.1f;
	}
	if (Key== '6')  // ������� ������� ������ ������?
	{
	 yspeed+=0.1f;      // ���� ���, �� �������� yspeed
	}
	if (Key=='4')   // ������� ������� ����� ������?
	{
	 yspeed-=0.1f;      // ���� ���, �� �������� yspeed
	}

	if (Key == 'A' || Key == 'a' || Key == '�' || Key == '�')
	{
		z-=1.0f;
	}

	if (Key == '�' || Key == '�' || Key == 'Z' || Key == 'z')
	{
		z+=1.0f;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if (Form1->Panel2->Visible) {

	double r1, r2, h1, h2, w1, w2, p=-999.0;
	bool down = false;
	double t;

	if (Form1->Edit1->Text.Length() > 0) {
		r1 = StrToFloat(Form1->Edit1->Text);
		if (r1 < 1.0 || r1 > 100.0) {
			Application->MessageBoxW(L"������ �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
			Form1->Edit1->SetFocus();
			return;
		}
	}
	else
	{
		Application->MessageBoxW(L"������ �������� ����:\n�������� �� �������", L"������ ����� ������");
		Form1->Edit1->SetFocus();
		return;
	}


	if (Form1->Edit2->Text.Length() > 0) {
		r2 = StrToFloat(Form1->Edit2->Text);
		if (r2 < 1.0 || r2 > 100.0) {
			Application->MessageBoxW(L"������ �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
			Form1->Edit2->SetFocus();
			return;
		}
	}
	else
	{
		Application->MessageBoxW(L"������ �������� ����:\n�������� �� �������", L"������ ����� ������");
		Form1->Edit2->SetFocus();
		return;
	}



	if (Form1->Edit3->Text.Length() > 0) {
		h1 = StrToFloat(Form1->Edit3->Text);
		if (h1 < 5.0 || h1 > 500.0) {
			Application->MessageBoxW(L"����� �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
			Form1->Edit3->SetFocus();
			return;
		}
	}
	else
	{
		Application->MessageBoxW(L"����� �������� ����:\n�������� �� �������", L"������ ����� ������");
		Form1->Edit3->SetFocus();
		return;
	}


	if (Form1->Edit4->Text.Length() > 0) {
		h2 = StrToFloat(Form1->Edit4->Text);
		if (h2 < 5.0 || h2 > 500.0) {
			Application->MessageBoxW(L"����� �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
			Form1->Edit4->SetFocus();
			return;
		}
	}
	else
	{
		Application->MessageBoxW(L"����� �������� ����:\n�������� �� �������", L"������ ����� ������");
		Form1->Edit4->SetFocus();
		return;
	}


	if (Form1->RadioButton1->Checked) {
		if (Form1->Edit5->Text.Length() > 0) {
		w1 = StrToFloat(Form1->Edit5->Text);
			if (w1 < 0.1 || w1 > 1000.0) {
				Application->MessageBoxW(L"������� �������� �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
				Form1->Edit5->SetFocus();
				return;
			}
		}
		else
		{
			Application->MessageBoxW(L"������� �������� �������� ����:\n�������� �� �������", L"������ ����� ������");
			Form1->Edit5->SetFocus();
			return;
		}


		if (Form1->Edit6->Text.Length() > 0) {
			w2 = StrToFloat(Form1->Edit6->Text);
			if (w2 < 0.1 || w2 > 1000.0) {
				Application->MessageBoxW(L"������� �������� �������� ����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
				Form1->Edit6->SetFocus();
				return;
			}
		}
		else
		{
			Application->MessageBoxW(L"������� �������� �������� ����:\n�������� �� �������", L"������ ����� ������");
			Form1->Edit6->SetFocus();
			return;
		}
		t = w1/w2;
	}



	if (Form1->RadioButton2->Checked) {
		if (Form1->Edit5->Text.Length() > 0) {
		p = StrToFloat(Form1->Edit5->Text);
			if (p < 0.02 || p > 50.0) {
				Application->MessageBoxW(L"������������ �����:\n �������� ������� �� ����������� ���������", L"������ ����� ������");
				Form1->Edit5->SetFocus();
				return;
			}
		}
		else
		{
			Application->MessageBoxW(L"������������ �����:\n�������� �� �������", L"������ ����� ������");
			Form1->Edit5->SetFocus();
			return;
		}
		t = p;
	}


	if (t>1.0) {
		down = true;
	}

	if (t < 0.02 || t > 50) {
		Application->MessageBoxW(L"�������� ������ �������� ������� �������,\n �������� ��������", L"������ ����� ������");
		if(down) Form1->Edit5->SetFocus(); else Form1->Edit6->SetFocus();
		return;
	}

	// sh1 = new sShaft(r1, h1, r1+(r1*0.1),
	if (t < 1.0) {
		normal = true;
		t = 1.0/t;
	}

	double t2 =  sqrt(t);
	double t1 = t / t2;

	double minshaft;
	double maxshaft;
	double minh;
	double maxh;




	if (r1 <= r2) {minshaft = r1; maxshaft = r2; minh = h1; maxh = h2; } else {minh = h2; maxh = h1; minshaft = r2; maxshaft = r1;}
	double k = 0.6;

	double e = maxshaft / minshaft;
	if ((maxshaft - minshaft) > 20)
		{
			l = ((minshaft + maxshaft) * 4 + (maxshaft - minshaft) * 0.8)  ;// * t;
			k = 0.6;
		}
	else
		{
			l = ((minshaft + maxshaft) * 4 + (maxshaft - minshaft) * 4) * 1.2; ;
			k = 0.53;
		}

	double l1, l2;
	l1 = l*k; l2 = l*(1.0-k);

	double minr = l2 / (t2+1.0)	;
	double sr2 = l2 - minr;


	double sr1 = l1 / (t1+1.0);
	double maxr = l1 - sr1;

	z  = l*1.2;



	double ww1 = maxr*0.2;
	double ww2 = minr*0.3;
	if (ww2 < 3.0) {
		ww2 = 3.0;
	}
	if (ww1 < 3.0) {
		ww1 = 3.0;
	}

	double mod1 = 0.7;
	double mod2 = 0.5;


	sh1 = new sShaft(maxshaft, maxh, maxr, ww1, mod1);
	sp = new sPart(sr1, ww1, mod1, sr2, ww2, mod2);
	sh2 = new sShaft(minshaft, minh, minr, ww2, mod2, sh1->cog->w);


	double tm = (double) sh1->cog->z/sp->cog1->z;
	tm *= (double)  sp->cog2->z/sh2->cog->z;


	Form1->Label16->Caption = FloatToStrF(tm, ffGeneral, 4, 4);
	Form1->Label18->Caption = FloatToStrF( (fabs(t - tm)/ double(t) * 100.0), ffGeneral,2, 2) + "%";

	Form1->Panel2->Visible = false;
	sh1->cog->angle =  0 ;
	sh1->cog->wSpeed = Form1->TrackBar1->Position / 5.0;
	sh1->cog->clock = false;

	double tmp = ( (1.0 - (sp->cog1->z % 4) / 4.0) * ((double) (360.0) / (sp->cog1->z)));
	double tmp2 = ( ((sh1->cog->z % 4) / 4.0) * ((double) (360.0) / (sp->cog1->z)));

	sp->cog1->angle =  (0.5* (double) (180.0) / (sp->cog1->z)) + tmp + tmp2 ;
	sp->cog2->angle = 0.0;


	sp->cog2->wSpeed =  (double) (sh1->cog->wSpeed * sh1->cog->z) / sp->cog1->z  ;
	sp->cog1->wSpeed = sp->cog2->wSpeed;
	sp->cog1->clock = true;
	sp->cog2->clock = true;

	tmp = ( (1.0 - (sh2->cog->z % 4) / 4.0) * ((double) (360.0) / (sh2->cog->z)));
	tmp2 = ( ((sp->cog2->z % 4) / 4.0) * ((double) (360.0) / (sh2->cog->z)));

	sh2->cog->angle =  (0.5*0.5*(double) (360.0) / (sh2->cog->z)) + tmp + tmp2;
	//sh2.cog->angle = sh2.cog->angle + (sh2.cog->angle/2.0);
	sh2->cog->wSpeed = (double) (sp->cog2->z * sp->cog2->wSpeed )/ sh2->cog->z;
	sh2->cog->clock = false;
	ShowWindow(Form1->Handle,SW_MAXIMIZE);

	Form1->Button1->Caption = "������ ����� ���������";
	Form1->Timer1->Enabled = true;
	Form1->BorderStyle = TFormBorderStyle::bsSizeable;
	TBorderIcons tempBI = BorderIcons;
	tempBI << biMaximize;
	Form1->BorderIcons = tempBI;
	}
	else
	{

		Form1->Timer1->Enabled = false;
        ShowWindow(Form1->Handle,SW_NORMAL);
		Form1->Panel2->Visible = true;
		Form1->Button1->Caption = "���������� � ��������� ��������";
		Form1->Width = 549;
		Form1->Height = 410;
		Form1->BorderStyle = TFormBorderStyle::bsSingle;
		TBorderIcons tempBI = BorderIcons;
		tempBI >> biMaximize;
		Form1->BorderIcons = tempBI;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar1Change(TObject *Sender)
{
	 //sh1.cog->angle =  Form1->TrackBar1->Position / 10.0;

			Form1->Label13->Caption = IntToStr(Form1->TrackBar1->Position) + "%";
			sh1->cog->wSpeed = Form1->TrackBar1->Position / 5.0;
			sp->cog2->wSpeed =  (double) (sh1->cog->wSpeed * sh1->cog->z) / sp->cog1->z;
			sp->cog1->wSpeed = sp->cog2->wSpeed;
			sh2->cog->wSpeed = (double) (sp->cog2->z * sp->cog2->wSpeed )/ sh2->cog->z;


}
//---------------------------------------------------------------------------




void __fastcall TForm1::FormMouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled)
{
	if (z < l*1.4 && z < 800) z+=1;
	//b += 0.05;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled)
{
	if (z > 30) z-=1;
	b -= 0.05;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Panel1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
		GetCursorPos(&mousexy);
		px = mousexy.x;
		py = mousexy.y;
		d = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Panel1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	d = false;
}
//---------------------------------------------------------------------------

bool CheckKey( wchar_t Key, UnicodeString Text)
{
	if ( (Key >= '0' && Key <= '9') || Key == '.' || Key == VK_BACK) {
		if (Key == '.' &&  Text.Pos(".") > 0) {
			return false;
		}
		return true;
	}
	return false;
}



void __fastcall TForm1::Edit1KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Form1->Edit1->Text)) Key = 0;
}
//---------------------------------------------------------------------------





void __fastcall TForm1::Edit2KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Edit2->Text)) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit3KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Form1->Edit3->Text)) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit4KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Form1->Edit4->Text)) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit5KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Form1->Edit5->Text)) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit6KeyPress(TObject *Sender, System::WideChar &Key)
{
	if (!CheckKey(Key, Form1->Edit6->Text)) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar1Enter(TObject *Sender)
{
	Form1->Panel1->SetFocus();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::RadioButton2Click(TObject *Sender)
{
	Form1->Edit6->Visible = false;
	Form1->Label9->Visible = false;
	Form1->Label1->Visible = false;
	Form1->Label8->Visible = false;
	Form1->Label10->Visible = true;
	Form1->Edit5->Text = "4";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton1Click(TObject *Sender)
{
	Form1->Edit6->Visible = true;
	Form1->Label9->Visible = true;
	Form1->Label1->Visible = true;
	Form1->Label8->Visible = true;
	Form1->Label10->Visible = false;
	Form1->Edit5->Text = "400";
	Form1->Edit6->Text = "100";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight,
          bool &Resize)
{
	if (NewWidth < 757 && Form1->Panel2->Visible == false) {
		NewWidth = 757;
	}
	if (NewHeight < 450 && Form1->Panel2->Visible == false) {
		NewHeight = 450;
	}
}
//---------------------------------------------------------------------------
void CreateInfo(TStringGrid *SG)
{
  SG->Cells[0][0] = "    ��������� ";
  SG->Cells[0][1] = " ������ ������ [��]";
  SG->Cells[0][2] = " ������ ������ [��]";
  SG->Cells[0][3] = " ���������� ������ [��]";
  SG->Cells[0][4] = " ������ ���������� ";
  SG->Cells[0][5] = " ������ ���� [��]";
  SG->Cells[0][6] = " ��������� ����� ������ [��]";
  SG->DefaultRowHeight = (Form2->ClientHeight-40) / 7.0;
  SG->DefaultColWidth = (Form2->ClientWidth-200)/4;
  SG->ColWidths[0] = 180;
  SG->Cells[1][0] = " ������� ������ ";
  SG->Cells[2][0] = " ������� ������ ";
  SG->Cells[3][0] = " ������� ����� ";
  SG->Cells[4][0] = " ������ ����� ";

  int a = 1;
  int b = 2;

  if (!normal) {

	a = 2;
	b = 1;

  }


  SG->Cells[a][1] = FloatToStrF(sh1->cog->r, ffGeneral, 4, 4);
  SG->Cells[b][1] = FloatToStrF(sh2->cog->r, ffGeneral, 4, 4);
  SG->Cells[3][1] = FloatToStrF(sp->cog1->r, ffGeneral, 4, 4);
  SG->Cells[4][1] = FloatToStrF(sp->cog2->r, ffGeneral, 4, 4);

  SG->Cells[a][2] = FloatToStrF(sh1->cog->w, ffGeneral, 4, 4);
  SG->Cells[b][2] = FloatToStrF(sh2->cog->w, ffGeneral, 4, 4);
  SG->Cells[3][2] = FloatToStrF(sp->cog1->w, ffGeneral, 4, 4);
  SG->Cells[4][2] = FloatToStrF(sp->cog2->w, ffGeneral, 4, 4);

  SG->Cells[a][3] = sh1->cog->z;
  SG->Cells[b][3] = sh2->cog->z;
  SG->Cells[3][3] = sp->cog1->z;
  SG->Cells[4][3] = sp->cog2->z;

  SG->Cells[a][4] = FloatToStrF(sh1->cog->m, ffGeneral, 2, 2);
  SG->Cells[b][4] = FloatToStrF(sh2->cog->m, ffGeneral, 2, 2);
  SG->Cells[3][4] = FloatToStrF(sp->cog1->m, ffGeneral, 2, 2);
  SG->Cells[4][4] = FloatToStrF(sp->cog2->m, ffGeneral, 2, 2);

  SG->Cells[a][5] = FloatToStrF(sh1->cog->h, ffGeneral, 4, 4);
  SG->Cells[b][5] = FloatToStrF(sh2->cog->h, ffGeneral, 4, 4);
  SG->Cells[3][5] = FloatToStrF(sp->cog1->h, ffGeneral, 4, 4);
  SG->Cells[4][5] = FloatToStrF(sp->cog2->h, ffGeneral, 4, 4);

  SG->Cells[a][6] = FloatToStrF(sh1->cog->c, ffGeneral, 4, 4);
  SG->Cells[b][6] = FloatToStrF(sh2->cog->c, ffGeneral, 4, 4);
  SG->Cells[3][6] = FloatToStrF(sp->cog1->c, ffGeneral, 4, 4);
  SG->Cells[4][6] = FloatToStrF(sp->cog2->c, ffGeneral, 4, 4);
}



void __fastcall TForm1::Button2Click(TObject *Sender)
{
	//
	Form1->Enabled = false;
	Form2->Visible = true;
	CreateInfo(Form2->StringGrid1);
	if (!normal)
	{
		sh2->cog->DrawPlan(Form2->Image1, sh2->r1, sh2->h1);
		sh1->cog->DrawPlan(Form2->Image2, sh1->r1, sh1->h1);
	}
	else
	{
		sh2->cog->DrawPlan(Form2->Image2, sh2->r1, sh2->h1);
		sh1->cog->DrawPlan(Form2->Image1, sh1->r1, sh1->h1);
	}
}
//---------------------------------------------------------------------------


