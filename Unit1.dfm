object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1077#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1076#1074#1091#1093#1089#1090#1091#1087#1077#1085#1095#1072#1090#1086#1075#1086' '#1079#1091#1073#1095#1072#1090#1086#1075#1086' '#1088#1077#1076#1091#1082#1090#1086#1088#1072
  ClientHeight = 371
  ClientWidth = 533
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 533
    Height = 371
    Align = alClient
    TabOrder = 2
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 531
      Height = 295
      Align = alClient
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnMouseDown = Panel1MouseDown
      OnMouseUp = Panel1MouseUp
    end
    object Panel5: TPanel
      Left = 1
      Top = 296
      Width = 531
      Height = 74
      Align = alBottom
      TabOrder = 1
      object Label15: TLabel
        Left = 312
        Top = 16
        Width = 168
        Height = 13
        Caption = #1055#1077#1088#1077#1076#1072#1090#1086#1095#1085#1086#1077' '#1095#1080#1089#1083#1086' '#1088#1077#1076#1091#1082#1090#1086#1088#1072':'
      end
      object Label16: TLabel
        Left = 486
        Top = 16
        Width = 6
        Height = 13
        Caption = '4'
      end
      object Label17: TLabel
        Left = 312
        Top = 40
        Width = 182
        Height = 13
        Caption = #1054#1090#1082#1083#1086#1085#1077#1085#1080#1077' '#1087#1077#1088#1077#1076#1072#1090#1086#1095#1085#1086#1075#1086' '#1095#1080#1089#1083#1072': '
      end
      object Label18: TLabel
        Left = 495
        Top = 40
        Width = 17
        Height = 13
        Caption = '0%'
      end
      object GroupBox2: TGroupBox
        Left = 6
        Top = 2
        Width = 295
        Height = 64
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1074#1088#1072#1097#1077#1085#1080#1103
        TabOrder = 0
        object Label11: TLabel
          Left = 4
          Top = 24
          Width = 17
          Height = 13
          Caption = '0%'
        end
        object Label12: TLabel
          Left = 227
          Top = 24
          Width = 29
          Height = 13
          Caption = '100%'
        end
        object Label13: TLabel
          Left = 130
          Top = 48
          Width = 23
          Height = 13
          Caption = '10%'
        end
        object Label14: TLabel
          Left = 25
          Top = 48
          Width = 99
          Height = 13
          Caption = #1058#1077#1082#1091#1097#1077#1077' '#1079#1085#1072#1095#1077#1085#1080#1077':'
        end
        object TrackBar1: TTrackBar
          Left = 21
          Top = 19
          Width = 207
          Height = 27
          Max = 100
          Position = 10
          TabOrder = 0
          ThumbLength = 15
          OnChange = TrackBar1Change
          OnEnter = TrackBar1Enter
        end
      end
      object Button2: TButton
        Left = 527
        Top = 11
        Width = 131
        Height = 25
        Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1088#1077#1076#1091#1082#1090#1086#1088#1072
        TabOrder = 1
        OnClick = Button2Click
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 533
    Height = 371
    Align = alClient
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object Label2: TLabel
      Left = 32
      Top = 80
      Width = 95
      Height = 19
      Caption = #1042#1077#1076#1091#1097#1080#1081' '#1074#1072#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 322
      Top = 80
      Width = 95
      Height = 19
      Caption = #1042#1077#1076#1086#1084#1099#1081' '#1074#1072#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 117
      Width = 189
      Height = 13
      Caption = #1056#1072#1076#1080#1091#1089' '#1074#1077#1076#1091#1097#1077#1075#1086' '#1074#1072#1083#1072' (1 - 100) ['#1084#1084']:'
    end
    object Label5: TLabel
      Left = 288
      Top = 117
      Width = 141
      Height = 13
      Caption = #1056#1072#1076#1080#1091#1089' '#1074#1077#1076#1086#1084#1086#1075#1086' '#1074#1072#1083#1072' ['#1084#1084']:'
    end
    object Label6: TLabel
      Left = 16
      Top = 176
      Width = 185
      Height = 13
      Caption = #1044#1083#1080#1085#1072' '#1074#1077#1076#1091#1097#1077#1075#1086' '#1074#1072#1083#1072' (5 - 500) ['#1084#1084']:'
    end
    object Label7: TLabel
      Left = 288
      Top = 176
      Width = 137
      Height = 13
      Caption = #1044#1083#1080#1085#1072' '#1074#1077#1076#1086#1084#1086#1075#1086' '#1074#1072#1083#1072' ['#1084#1084']:'
    end
    object Label8: TLabel
      Left = 16
      Top = 238
      Width = 180
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1074#1088#1072#1097#1077#1085#1080#1103' '#1074#1077#1076#1091#1097#1077#1075#1086' '#1074#1072#1083#1072' '
    end
    object Label9: TLabel
      Left = 288
      Top = 254
      Width = 223
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1074#1088#1072#1097#1077#1085#1080#1103' '#1074#1077#1076#1086#1084#1086#1075#1086' '#1074#1072#1083#1072' ['#1086#1073'/'#1084#1080#1085']:'
    end
    object Label1: TLabel
      Left = 20
      Top = 254
      Width = 110
      Height = 13
      Caption = '(0.1 - 1000)  ['#1086#1073'/'#1084#1080#1085']:'
    end
    object Label10: TLabel
      Left = 16
      Top = 254
      Width = 219
      Height = 13
      Caption = #1055#1077#1088#1077#1076#1072#1090#1086#1095#1085#1086#1077' '#1095#1080#1089#1083#1086' '#1088#1077#1076#1091#1082#1090#1086#1088#1072' (0.02 - 50)'
      Visible = False
    end
    object Panel3: TPanel
      Left = 259
      Top = 80
      Width = 4
      Height = 241
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 16
      Top = 136
      Width = 225
      Height = 21
      MaxLength = 5
      TabOrder = 1
      Text = '5'
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 288
      Top = 136
      Width = 223
      Height = 21
      MaxLength = 5
      TabOrder = 2
      Text = '10'
      OnKeyPress = Edit2KeyPress
    end
    object Edit3: TEdit
      Left = 16
      Top = 195
      Width = 225
      Height = 21
      MaxLength = 5
      TabOrder = 3
      Text = '20'
      OnKeyPress = Edit3KeyPress
    end
    object Edit4: TEdit
      Left = 288
      Top = 195
      Width = 223
      Height = 21
      MaxLength = 5
      TabOrder = 4
      Text = '30'
      OnKeyPress = Edit4KeyPress
    end
    object Edit5: TEdit
      Left = 16
      Top = 273
      Width = 225
      Height = 21
      MaxLength = 5
      TabOrder = 5
      Text = '400'
      OnKeyPress = Edit5KeyPress
    end
    object Edit6: TEdit
      Left = 288
      Top = 273
      Width = 223
      Height = 21
      MaxLength = 5
      TabOrder = 6
      Text = '100'
      OnKeyPress = Edit6KeyPress
    end
    object GroupBox1: TGroupBox
      Left = 32
      Top = 16
      Width = 465
      Height = 58
      Caption = #1057#1087#1086#1089#1086#1073' '#1079#1072#1076#1072#1085#1080#1103' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082'  '#1088#1077#1076#1091#1082#1090#1086#1088#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      object RadioButton1: TRadioButton
        Left = 24
        Top = 24
        Width = 162
        Height = 17
        Caption = #1063#1072#1089#1090#1086#1090#1099' '#1074#1088#1072#1097#1077#1085#1080#1103' '#1074#1072#1083#1086#1074
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = True
        OnClick = RadioButton1Click
      end
      object RadioButton2: TRadioButton
        Left = 216
        Top = 24
        Width = 201
        Height = 17
        Caption = #1055#1077#1088#1077#1076#1072#1090#1086#1095#1085#1086#1077' '#1095#1080#1089#1083#1086' '#1088#1077#1076#1091#1082#1090#1086#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = RadioButton2Click
      end
    end
  end
  object Button1: TButton
    Left = 336
    Top = 331
    Width = 189
    Height = 25
    Caption = #1056#1072#1089#1089#1095#1080#1090#1072#1090#1100' '#1080' '#1087#1086#1089#1090#1088#1086#1080#1090#1100' '#1088#1077#1076#1091#1082#1090#1086#1088
    TabOrder = 0
    OnClick = Button1Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 20
    OnTimer = Timer1Timer
    Left = 704
    Top = 440
  end
end
