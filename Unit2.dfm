object Form2: TForm2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1088#1077#1076#1091#1082#1090#1086#1088#1072
  ClientHeight = 421
  ClientWidth = 753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 753
    Height = 421
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 749
    ExplicitHeight = 415
    object TabSheet1: TTabSheet
      Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      ExplicitWidth = 741
      ExplicitHeight = 387
      object StringGrid1: TStringGrid
        Left = 0
        Top = 0
        Width = 745
        Height = 393
        Align = alClient
        RowCount = 7
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected]
        ParentFont = False
        ScrollBars = ssNone
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1063#1077#1088#1090#1077#1078' '#1074#1077#1076#1091#1097#1077#1075#1086' '#1082#1086#1083#1077#1089#1072
      ImageIndex = 1
      ExplicitWidth = 741
      ExplicitHeight = 387
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 745
        Height = 393
        Align = alClient
        ExplicitLeft = 48
        ExplicitTop = 16
        ExplicitWidth = 105
        ExplicitHeight = 105
      end
      object Button1: TButton
        Left = 656
        Top = 352
        Width = 75
        Height = 25
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        TabOrder = 0
        OnClick = Button1Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1063#1077#1088#1090#1077#1078' '#1074#1077#1076#1086#1084#1086#1075#1086' '#1082#1086#1083#1077#1089#1072
      ImageIndex = 2
      ExplicitWidth = 741
      ExplicitHeight = 387
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 745
        Height = 393
        Align = alClient
        ExplicitLeft = 144
        ExplicitTop = 96
        ExplicitWidth = 105
        ExplicitHeight = 105
      end
      object Button2: TButton
        Left = 656
        Top = 352
        Width = 75
        Height = 25
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        TabOrder = 0
        OnClick = Button2Click
      end
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 696
    Top = 32
  end
end
